package triumphit.flash.light;

import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

public class MainActivity extends AppCompatActivity {

    ImageButton onOff;
    boolean isOn = false;
    static Camera cam;
    static Camera.Parameters p;
    private static Handler handler;
    private static Runnable updateTask;
    private static boolean handlerCheck = false;
    boolean isStarted = false;
    private StartAppAd startAppAd = new StartAppAd(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final boolean b = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        onOff = (ImageButton) findViewById(R.id.imageButton);
        StartAppSDK.init(this, "207904228", true);

//        handler = new Handler();
//        updateTask = new Runnable() {
//            @TargetApi(Build.VERSION_CODES.FROYO)
//            @Override
//            public void run() {
//                if(isStarted){
//                    if(isOn){
//                        ledOff();
//                        isOn = false;
//                    }else{
//                        ledOn();
//                        isOn = true;
//                    }
//                }else ledOff();
//                handler.postDelayed(this, 1000);
//            }
//        };
//        if (!handlerCheck) {
//            handler.postDelayed(updateTask, 0);
//            handlerCheck = true;
//        }


        onOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isStarted == false){
                    ledOn();
                    onOff.setBackgroundResource(R.drawable.yeallow);
                }else{
                    isStarted = false;
                    ledOff();
                    onOff.setBackgroundResource(R.drawable.white);
                }
            }
        });
    }
    void ledOn(){
        cam = Camera.open();
        p = cam.getParameters();
        p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        cam.setParameters(p);
        cam.startPreview();
    }
    void ledOff(){
        if(cam != null){
            cam.stopPreview();
            cam.release();
        }

    }

    @Override
    public void onBackPressed() {
        startAppAd.showAd();
        super.onBackPressed();
    }
}
